# -*- coding: utf-8 -*-


from leasing.models import Car, Company, PriceList
from django_pandas.io import read_frame
from django.views.static import serve
import os
from django.shortcuts import render
from django.core import serializers
import json
from django.http import Http404, HttpResponse


def cars_from_active_companies(request):
    # cars_active = Car.objects.filter(company__is_active=True).distinct()
    df = read_frame(Car.objects.cars_from_active_companies_query(), fieldnames=['car_name', 'company__company_name', 'price'])
    df.to_excel("output1.xlsx")
    filepath = 'output1.xlsx'
    return serve(request, os.path.basename(filepath), os.path.dirname(filepath))
    # return HttpResponse(cars_active)

def cars_from_active_companies_show(request):
    data = serializers.serialize('json', Car.objects.cars_from_active_companies_query(), fields=('car_name', 'company__company_name', 'price'))
    print type(HttpResponse(data))
    return HttpResponse(data)

def prices_for_car(request,car_name):
    # list_of_prices = PriceList.objects.filter(car__car_name=car_name)
    df = read_frame(PriceList.objects.prices_for_car_query(car_name), fieldnames=['company__company_name', 'price_at_company'])
    df.to_excel("output2.xlsx")
    filepath = 'output2.xlsx'
    return serve(request, os.path.basename(filepath), os.path.dirname(filepath))
    # return HttpResponse(list_of_prices)


def prices_for_car_show(request,car_name):
    data = serializers.serialize('json', PriceList.objects.prices_for_car_query(car_name), use_natural_foreign_keys=True)
    return HttpResponse(data)



def prices_for_company(request,company_name):
    # price_for_company = PriceList.objects.filter(company__company_name=company_name)
    df = read_frame(PriceList.objects.prices_for_company_query(company_name), fieldnames=['car__car_name','price_at_company'])
    df.to_excel("output3.xlsx")
    filepath = 'output3.xlsx'
    return serve(request, os.path.basename(filepath), os.path.dirname(filepath))
    # return HttpResponse(price_for_company)

def prices_for_company_show(request,company_name):
    data = serializers.serialize('json', PriceList.objects.prices_for_company_query(company_name), use_natural_foreign_keys=True)
    return HttpResponse(data)


def active_companies_with_yud(request):
    # companies = Company.objects.filter(is_active=True, company_name__contains="י").distinct()
    df = read_frame(Company.objects.active_companies_with_yud_query(), fieldnames=['company_name', 'city'])
    df.to_excel("output4.xlsx")
    filepath = 'output4.xlsx'
    return serve(request, os.path.basename(filepath), os.path.dirname(filepath))
    # return HttpResponse(companies)

def active_companies_with_yud_show(request):
    data = serializers.serialize('json', Company.objects.active_companies_with_yud_query(), fields=('company_name','city', 'phone_number'))
    return HttpResponse(data)

def min_price_for_each_company(request):
    df = read_frame(PriceList.objects.min_price_for_each_company_query())
    df.to_excel("output5.xlsx")
    filepath = 'output5.xlsx'
    return serve(request, os.path.basename(filepath), os.path.dirname(filepath))

def min_price_for_each_company_show(request):
    data = serializers.serialize('json', PriceList.objects.min_price_for_each_company_query(), use_natural_foreign_keys=True)
    return HttpResponse(data)


def index(request):
    return render(request, "index.html")


# def cars_from_active_companies1(request):
#     if request.is_ajax():
#         detailes = ['car_name', 'company','price_at_company']
#         data = json.dumps(detailes)
#         return HttpResponse(data, content_type='application/json')
#     else:
#         raise Http404
