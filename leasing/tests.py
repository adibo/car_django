# -*- coding: utf-8 -*-
import django
django.setup()
from django.test import TestCase, Client
from leasing.models import Car, Company, PriceList
import pandas as pd
from pandas.util.testing import assert_frame_equal


class TestsForLeasing(TestCase):

    def setUp(self):
        self.client = Client()
        self.company1 = Company(company_name=u"חברת ליסינג", city=u"חיפה", phone_number=u"04-2431689", is_active=True)
        self.company1.save()
        self.company2 = Company(company_name=u"company2", city=u"אשדוד", phone_number=u"08-5463289", is_active=False)
        self.company2.save()
        self.company3 = Company(company_name=u"company3", city=u"ירושלים", phone_number=u"02-4253791", is_active=True)
        self.company3.save()
        self.car1 = Car(car_name=u"car1", price=80000)
        self.car1.save()
        self.car2 = Car(car_name=u"car2", price=30000)
        self.car2.save()
        self.car3 = Car(car_name=u"car3", price=100000)
        self.car3.save()
        self.price1 = PriceList(car=self.car1, company=self.company1, price_at_company=2000)
        self.price1.save()
        self.price2 = PriceList(car=self.car2, company=self.company2, price_at_company=3000)
        self.price2.save()
        self.price3 = PriceList(car=self.car3, company=self.company3, price_at_company=2500)
        self.price3.save()


    def test_cars_from_active_companies_length(self):
        query_output = Car.objects.filter(company__is_active=True).distinct()
        self.assertEqual(len(query_output), 2)

    def test_cars_from_active_companies_excel(self):
        df_to_assert = pd.DataFrame({
            'car_name': [u"car1", u'car3'],
            'company__company_name': [u'חברת ליסינג', u'company3'],
            'price': [80000, 100000],
        })
        self.client.get("/leasing/cars_from_active_companies")
        df = pd.read_excel('output1.xlsx')
        assert_frame_equal(df_to_assert.sort(axis=1), df.sort(axis=1), check_names=True)

    def test_query_cars_from_active_companies(self):
        query = self.client.get("/leasing/cars_from_active_companies")
        self.assertEqual(query.status_code, 200)

    def test_prices_for_car_length(self):
        query_output = PriceList.objects.filter(car__car_name=u"car1")
        self.assertEqual(len(query_output), 1)

    def test_prices_for_car_excel(self):
        df_to_assert = pd.DataFrame({
            'company__company_name': [u'company2'],
            'price_at_company': [3000],
        })
        self.client.get("/leasing/prices_for_car/car2/")
        df = pd.read_excel('output2.xlsx')
        assert_frame_equal(df_to_assert.sort(axis=1), df.sort(axis=1), check_names=True)

    def test_query_prices_for_car(self):
        query = self.client.get("/leasing/prices_for_car/car2/")
        self.assertEqual(query.status_code, 200)

    def test_prices_for_company_length(self):
        query_output = PriceList.objects.filter(company__company_name=u"company2")
        self.assertEqual(len(query_output), 1)

    def test_prices_for_company_excel(self):
        df_to_assert = pd.DataFrame({
            'car__car_name': [u'car3'],
            'price_at_company': [2500],
        })
        self.client.get("/leasing/prices_for_company/company3/")
        df = pd.read_excel('output3.xlsx')
        assert_frame_equal(df_to_assert.sort(axis=1), df.sort(axis=1), check_names=True)

    def test_query_prices_for_company(self):
        query = self.client.get("/leasing/prices_for_company/company3/")
        self.assertEqual(query.status_code, 200)

    def test_active_companies_with_yud_length(self):
        query_output = Company.objects.filter(is_active=True, company_name__contains="י").distinct()
        self.assertEqual(len(query_output), 1)

    def test_active_companies_with_yud_excel(self):
        df_to_assert = pd.DataFrame({
            'company_name': [u'חברת ליסינג'],
            'city': [u'חיפה'],
        })
        self.client.get("/leasing/active_companies_with_yud")
        df = pd.read_excel('output4.xlsx')
        assert_frame_equal(df_to_assert.sort(axis=1), df.sort(axis=1), check_names=True)

    def test_active_companies_with_yud(self):
        query = self.client.get("/leasing/active_companies_with_yud")
        self.assertEqual(query.status_code, 200)


