from django.conf.urls import patterns, include, url
from . import views
from leasing.viewsets.car_viewset import CarViewSet
from leasing.viewsets.company_viewset import CompanyViewSet
from leasing.viewsets.pricelist_viewset import PriceListViewSet
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedDefaultRouter


admin.autodiscover()
router = ExtendedDefaultRouter()
(
    router.register('cars', CarViewSet, base_name='cars'),
    router.register('companies', CompanyViewSet, base_name='companies'),
    router.register('prices', PriceListViewSet, base_name='prices'),
    router.register('cars', CarViewSet, base_name='cars')
        .register('companies', CompanyViewSet, base_name='companies', parents_query_lookups=['car']),
    router.register('companies', CompanyViewSet, base_name='companies')
        .register('cars', CarViewSet, base_name='cars', parents_query_lookups=['company']),
)


urlpatterns = [
    url(r'^cars_from_active_companies$', views.cars_from_active_companies, name='cars_from_active_companies'),
    url(r'^cars_from_active_companies_show$', views.cars_from_active_companies_show, name='cars_from_active_companies_show'),
    url(r'^prices_for_car/(?P<car_name>.+)/$', views.prices_for_car, name='prices_for_car'),
    url(r'^prices_for_car_show/(?P<car_name>.+)/$', views.prices_for_car_show, name='prices_for_car_show'),
    url(r'^prices_for_company/(?P<company_name>.+)/$', views.prices_for_company, name='prices_for_company'),
    url(r'^prices_for_company_show/(?P<company_name>.+)/$', views.prices_for_company_show, name='prices_for_company_show'),
    url(r'^active_companies_with_yud$', views.active_companies_with_yud, name='active_companies_with_yud'),
    url(r'^active_companies_with_yud_show$', views.active_companies_with_yud_show, name='active_companies_with_yud_show'),
    url(r'^min_price_for_each_company$', views.min_price_for_each_company, name='min_price_for_each_company'),
    url(r'^min_price_for_each_company_show$', views.min_price_for_each_company_show, name='min_price_for_each_company_show'),

    url('', include(router.urls)),
    url(r'^index$', views.index, name='index')
]