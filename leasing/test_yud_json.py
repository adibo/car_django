# -*- coding: utf-8 -*-
import django
django.setup()
from django.test import TestCase, Client
from leasing.models import Car, Company, PriceList
import json


class TestsForLeasing2(TestCase):

    def setUp(self):
        self.client = Client()
        self.company1 = Company(company_name=u"יויו", city="Haifa", phone_number="04-2431689", is_active=True)
        self.company1.save()
        self.company2 = Company(company_name="company2", city="Ashdod", phone_number="08-5463289", is_active=False)
        self.company2.save()
        self.company3 = Company(company_name="company3", city="Tel Aviv", phone_number="02-4253791", is_active=True)
        self.company3.save()
        self.car1 = Car(car_name="car1", price=80000)
        self.car1.save()
        self.car2 = Car(car_name="car2", price=30000)
        self.car2.save()
        self.car3 = Car(car_name="car3", price=100000)
        self.car3.save()
        self.price1 = PriceList(car=self.car1, company=self.company1, price_at_company=2000)
        self.price1.save()
        self.price2 = PriceList(car=self.car2, company=self.company2, price_at_company=3000)
        self.price2.save()
        self.price3 = PriceList(car=self.car3, company=self.company3, price_at_company=2500)
        self.price3.save()



    def test_active_companies_with_yud_show_json(self):
        response = self.client.get('/leasing/active_companies_with_yud_show')
        parsed_data_from_response = json.loads(response.content)
        self.assertEqual(parsed_data_from_response[0]['fields']['phone_number'], "04-2431689")









