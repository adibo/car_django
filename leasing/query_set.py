# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Min, F


class PriceListQuerySet(models.QuerySet):

    def prices_for_car_query(self, car_name):
        return self.filter(car__car_name=car_name)

    def prices_for_company_query(self, company_name):
        return self.filter(company__company_name=company_name)

    def min_price_for_each_company_query(self):
        # return self.values('company').annotate(min_price=Min('price_at_company'))
        # return self.values('company').annotate(min_price=Min('price_at_company'))
        return self.annotate(min_price=Min('company__pricelist__price_at_company'))\
            .filter(price_at_company=F('min_price')).only('car', 'company', 'price_at_company')


class CarQuerySet(models.QuerySet):

    def cars_from_active_companies_query(self):
        return self.filter(company__is_active=True).distinct()


class CompanyQuerySet(models.QuerySet):

    def active_companies_with_yud_query(self):
        return self.filter(is_active=True, company_name__contains="י").distinct()


