# -*- coding: utf-8 -*-
import django
django.setup()
from django.test import TestCase, Client
from leasing.models import Car, Company, PriceList
import pandas as pd
from pandas.util.testing import assert_frame_equal
from django.http import Http404, HttpResponse
import json
import  views


class TestsForLeasing2(TestCase):

    def setUp(self):
        self.client = Client()
        self.company1 = Company(company_name="company1", city="Haifa", phone_number="04-2431689", is_active=True)
        self.company1.save()
        self.company2 = Company(company_name="company2", city="Ashdod", phone_number="08-5463289", is_active=False)
        self.company2.save()
        self.company3 = Company(company_name="company3", city="Tel Aviv", phone_number="02-4253791", is_active=True)
        self.company3.save()
        self.car1 = Car(car_name="car1", price=80000)
        self.car1.save()
        self.car2 = Car(car_name="car2", price=30000)
        self.car2.save()
        self.car3 = Car(car_name="car3", price=100000)
        self.car3.save()
        self.price1 = PriceList(car=self.car1, company=self.company1, price_at_company=2000)
        self.price1.save()
        self.price2 = PriceList(car=self.car2, company=self.company2, price_at_company=3000)
        self.price2.save()
        self.price3 = PriceList(car=self.car3, company=self.company3, price_at_company=2500)
        self.price3.save()


    def test_cars_from_active_companies_show_status(self):
        data_from_response = self.client.get('/leasing/cars_from_active_companies_show')
        self.assertEqual(data_from_response.status_code,200)

    def test_cars_from_active_companies_show(self):
        data_from_response = self.client.get('/leasing/cars_from_active_companies_show')
        self.assertContains(data_from_response, "car1")
        self.assertContains(data_from_response, "car3")


    def test_prices_for_car_show(self):
        data_from_response = self.client.get('/leasing/prices_for_car_show/car1/')
        self.assertContains(data_from_response, "company1")
        self.assertContains(data_from_response, "car1")
        self.assertContains(data_from_response, "2000")


    def test_prices_for_car_show_status(self):
        data_from_response = self.client.get('/leasing/prices_for_car_show/car1/')
        self.assertEqual(data_from_response.status_code,200)


    def test_prices_for_company_show(self):
        data_from_response = self.client.get('/leasing/prices_for_company_show/company2/')
        self.assertContains(data_from_response, "company2")
        self.assertContains(data_from_response, "car2")
        self.assertContains(data_from_response, "3000")


    def test_prices_for_company_show_status(self):
        data_from_response = self.client.get('/leasing/prices_for_company_show/company2/')
        self.assertEqual(data_from_response.status_code,200)

    def test_active_companies_with_yud_show_status(self):
        data_from_response = self.client.get('/leasing/active_companies_with_yud_show')
        self.assertEqual(data_from_response.status_code,200)

    def test_min_price_for_each_company_show(self):
        data_from_response = self.client.get('/leasing/min_price_for_each_company_show')
        self.assertContains(data_from_response, "2500")

    def test_min_price_for_each_company_show_status(self):
        data_from_response = self.client.get('/leasing/min_price_for_each_company_show')
        self.assertEqual(data_from_response.status_code,200)







