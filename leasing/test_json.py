# -*- coding: utf-8 -*-
import django
django.setup()
from django.test import TestCase, Client
from leasing.models import Car, Company, PriceList
import json



class TestsForLeasing2(TestCase):

    def setUp(self):
        self.client = Client()
        self.company1 = Company(company_name="company1", city="Haifa", phone_number="04-2431689", is_active=True)
        self.company1.save()
        self.company2 = Company(company_name="company2", city="Ashdod", phone_number="08-5463289", is_active=False)
        self.company2.save()
        self.company3 = Company(company_name="company3", city="Tel Aviv", phone_number="02-4253791", is_active=True)
        self.company3.save()
        self.car1 = Car(car_name="car1", price=80000)
        self.car1.save()
        self.car2 = Car(car_name="car2", price=30000)
        self.car2.save()
        self.car3 = Car(car_name="car3", price=100000)
        self.car3.save()
        self.car4 = Car(car_name="car4", price=70000)
        self.car4.save()
        self.price1 = PriceList(car=self.car1, company=self.company1, price_at_company=2000)
        self.price1.save()
        self.price2 = PriceList(car=self.car2, company=self.company2, price_at_company=3000)
        self.price2.save()
        self.price3 = PriceList(car=self.car3, company=self.company3, price_at_company=2500)
        self.price3.save()
        self.price4 = PriceList(car=self.car4, company=self.company1, price_at_company=1500)
        self.price4.save()


    def test_cars_from_active_companies_show_json(self):
        response = self.client.get('/leasing/cars_from_active_companies_show')
        parsed_data_from_response = json.loads(response.content)
        self.assertEqual(parsed_data_from_response[0]['fields']['car_name'], "car1")
        self.assertEqual(parsed_data_from_response[1]['fields']['car_name'], "car3")
        self.assertEqual(parsed_data_from_response[2]['fields']['car_name'], "car4")


    def test_prices_for_car_show_json(self):
        response = self.client.get('/leasing/prices_for_car_show/car1/')
        parsed_data_from_response = json.loads(response.content)
        self.assertEqual(parsed_data_from_response[0]['fields']['price_at_company'], 2000)


    def test_prices_for_company_show_json(self):
        response = self.client.get('/leasing/prices_for_company_show/company2/')
        parsed_data_from_response = json.loads(response.content)
        self.assertEqual(parsed_data_from_response[0]['fields']['price_at_company'], 3000)


    def test_min_price_for_each_company_json(self):
        response = self.client.get('/leasing/min_price_for_each_company_show')
        parsed_data_from_response = json.loads(response.content)
        self.assertEqual(parsed_data_from_response[0]['fields']['price_at_company'], 3000)









