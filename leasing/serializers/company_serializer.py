from rest_framework import serializers
from leasing.models import Company


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('company_name', 'city', 'phone_number', 'is_active')