# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-10 11:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('leasing', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='city',
            field=models.CharField(choices=[('\u05d7\u05d9\u05e4\u05d4', '\u05d7\u05d9\u05e4\u05d4'), ('\u05ea\u05dc \u05d0\u05d1\u05d9\u05d1', '\u05ea\u05dc \u05d0\u05d1\u05d9\u05d1'), ('\u05d0\u05e9\u05d3\u05d5\u05d3', '\u05d0\u05e9\u05d3\u05d5\u05d3'), ('\u05e8\u05de\u05ea \u05d2\u05df', '\u05e8\u05de\u05ea \u05d2\u05df'), ('\u05d9\u05d4\u05d5\u05d3', '\u05d9\u05d4\u05d5\u05d3'), ('\u05d9\u05e8\u05d5\u05e9\u05dc\u05d9\u05dd', '\u05d9\u05e8\u05d5\u05e9\u05dc\u05d9\u05dd')], default='no choice', max_length=10),
        ),
    ]
