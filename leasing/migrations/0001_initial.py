# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-03 14:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('car_name', models.CharField(max_length=200)),
                ('price', models.IntegerField(default=0)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company_name', models.CharField(max_length=200)),
                ('city', models.CharField(choices=[('\u05d7\u05d9\u05e4\u05d4', '\u05d7\u05d9\u05e4\u05d4'), ('\u05dc\u05ea \u05d0\u05d1\u05d9\u05d1', '\u05ea\u05dc \u05d0\u05d1\u05d9\u05d1'), ('\u05d0\u05e9\u05d3\u05d5\u05d3', '\u05d0\u05e9\u05d3\u05d5\u05d3'), ('\u05e8\u05de\u05ea \u05d2\u05df', '\u05e8\u05de\u05ea \u05d2\u05df'), ('\u05d9\u05d4\u05d5\u05d3', '\u05d9\u05d4\u05d5\u05d3'), ('\u05d9\u05e8\u05d5\u05e9\u05dc\u05d9\u05dd', '\u05d9\u05e8\u05d5\u05e9\u05dc\u05d9\u05dd')], default='no choice', max_length=10)),
                ('phone_number', models.CharField(max_length=100)),
                ('is_active', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='PriceList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price_at_company', models.IntegerField(default=0)),
                ('car', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='leasing.Car')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='leasing.Company')),
            ],
        ),
        migrations.AddField(
            model_name='car',
            name='company',
            field=models.ManyToManyField(through='leasing.PriceList', to='leasing.Company'),
        ),
    ]
