# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from model_utils.models import TimeStampedModel
from query_set import CarQuerySet, PriceListQuerySet, CompanyQuerySet


class Company(models.Model):
    company_name = models.CharField(max_length=200)
    CITIES_OF_COMPANIES = (
        ('חיפה', "חיפה"),
        ('תל אביב', "תל אביב"),
        ("אשדוד", "אשדוד"),
        ("רמת גן", "רמת גן"),
        ("יהוד", "יהוד"),
        ("ירושלים", "ירושלים"),
    )
    city = models.CharField(max_length=10, choices=CITIES_OF_COMPANIES, default="no choice")
    phone_number = models.CharField(max_length=100)
    is_active = models.BooleanField(default=False)
    objects = CompanyQuerySet.as_manager()

    def __unicode__(self):
        return self.company_name

    def natural_key(self):
        return self.company_name


class Car(TimeStampedModel, models.Model):
    car_name = models.CharField(max_length=200)
    price = models.IntegerField(default=0)
    company = models.ManyToManyField('Company', through='PriceList')
    objects = CarQuerySet.as_manager()


    def __unicode__(self):
        return self.car_name

    def natural_key(self):
        return self.car_name


class PriceList (models.Model):
    car = models.ForeignKey(Car)
    company = models.ForeignKey(Company)
    price_at_company = models.IntegerField(default=0)
    objects = PriceListQuerySet.as_manager()


    def __unicode__(self):
        return self.car.car_name + " ," + self.company.company_name
