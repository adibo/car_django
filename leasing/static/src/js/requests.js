$('#show_cars_from_active_companies_button').click(function(e){
    e.preventDefault();

    $.ajax({
        url: '/leasing/cars_from_active_companies_show',
        method:'GET',
        contentType: false,
        processData: false,
        success: function(response) {
        console.log(response);
        var result_table ="";
        result_table += "<table class='tables' align='center' border='1'>   <tr>   <th>מחיר</th>  <th>שם הרכב</th> </tr>"
        var data = $.parseJSON(response)
        var car, price;
        for (i = 0; i < data.length; i++) {
        car = data[i].fields.car_name;
        price = data[i].fields.price;
            result_table += "<tr>  <td>" +price+ "</td> <td>" +car+ "</td> </tr>"
        }
        result_table += "</table>"

        $( "#results_div_cars_from_active_companies_show" ).empty().append(result_table);

        },
        error: function(errResponse) {
            console.log(errResponse);
        }
    })

});


$('#show_prices_for_car_button').click(function(e){
    e.preventDefault();

    var car_name = $('#car_name').val();

    $.ajax({
        url: ('/leasing/prices_for_car_show/' + car_name),
        method:'GET',
        contentType: false,
        processData: false,
        success: function(response) {
            var data = $.parseJSON(response);
            var result_table ="";
            result_table += "<table class='tables' align='center' border='1'>   <tr>   <th>מחיר</th>  <th>שם החברה</th> </tr>";
            var company, price;
            for (i = 0; i < data.length; i++) {
            company = data[i].fields.company;
            price = data[i].fields.price_at_company;
                result_table += "<tr>  <td>" +price+ "</td> <td>" +company+ "</td> </tr>";
            }
            result_table += "</table>";

            $( "#results_div_prices_for_car_show" ).empty().append(result_table);

        },
        error: function(errResponse) {
            console.log(errResponse);
        }
    })

});





$('#show_prices_for_company_button').click(function(e){
    e.preventDefault();

    var company_name = $('#company_name').val();


    $.ajax({
        url: ('/leasing/prices_for_company_show/' + company_name),
        method:'GET',
        contentType: false,
        processData: false,
        success: function(response) {
            console.log(response);
            var data = $.parseJSON(response);
            var result_table ="";
            result_table += "<table class='tables' align='center' border='1'>   <tr>   <th>מחיר</th>  <th>שם הרכב</th> </tr>"
            var car, price;
            for (i = 0; i < data.length; i++) {
            car = data[i].fields.car;
            price = data[i].fields.price_at_company;
                result_table += "<tr>  <td>" +price+ "</td> <td>" +car+ "</td> </tr>"
            }
            result_table += "</table>"

            $( "#results_div_prices_for_company_show" ).empty().append(result_table);

        },
        error: function(errResponse) {
            console.log(errResponse);
        }
    })
 });




$('#show_active_companies_with_yud_button').click(function(e){
    e.preventDefault();

    $.ajax({
        url: '/leasing/active_companies_with_yud_show',
        method:'GET',
        contentType: false,
        processData: false,
        success: function(response) {
        console.log(response);
        var result_table ="";
        result_table += "<table class='tables' align='center' border='1'>   <tr>   <th>טלפון</th>  <th>עיר</th> <th>שם החברה</th> </tr>"
        var data = $.parseJSON(response);
        var company_name, phone_number, city;
        for (i = 0; i < data.length; i++) {
        company = data[i].fields.company_name;
        city = data[i].fields.city;
        phone_number = data[i].fields.phone_number;
            result_table += "<tr>  <td>" +phone_number+ "</td> <td>" +city+ "</td> <td>" +company+ "</td> </tr>"
        }
        result_table += "</table>"

        $( "#result_div_active_companies_with_yud_show" ).empty().append(result_table);

        },
        error: function(errResponse) {
            console.log(errResponse);
        }
    })

});


$('#show_min_price_for_each_company_button').click(function(e){
    e.preventDefault();

    $.ajax({
        url: '/leasing/min_price_for_each_company_show',
        method:'GET',
        contentType: false,
        processData: false,
        success: function(response) {
            console.log(response);
        var result_table ="";
        result_table += "<table class='tables' align='center' border='1'>   <tr>   <th> מחיר</th>  <th>רכב</th> <th>שם החברה</th> </tr>"
        var data = $.parseJSON(response);
        var company, car, price;
        for (i = 0; i < data.length; i++) {
        company = data[i].fields.company;
        car = data[i].fields.car;
        price = data[i].fields.price_at_company;
            result_table += "<tr>  <td>" +price+ "</td> <td>" +car+ "</td> <td>" +company+ "</td> </tr>"
        }
        result_table += "</table>"

        $( "#results_div_min_price_for_each_company_show" ).empty().append(result_table);

        },
        error: function(errResponse) {
            console.log(errResponse);
        }
    })

});





